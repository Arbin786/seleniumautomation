package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProgressiveGetAQuoteClassObject {

	WebDriver driver = null;

	WebElement element = null;

	By enterZipCode = By.xpath("//input[contains(@type,'tel')]");

	By getAQuote = By.xpath("//input[contains(@type,'submit')]");

	public ProgressiveGetAQuoteClassObject(WebDriver driver) {

		this.driver = driver;
	}

	public void enterZipCode(String zip) throws InterruptedException

	{

		driver.findElement(enterZipCode).sendKeys(zip);

		Thread.sleep(1000);
	}

	public void getAQuote() throws InterruptedException {
		Thread.sleep(1000);
		ScreenShotsClassObject.captureScreenShots(driver, "GetAQuote Page");
		driver.findElement(getAQuote).click();

	}
}
