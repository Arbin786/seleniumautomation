package Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class ProgressiveAboutYourSelfClassObject {
	WebDriver driver = null;

	WebElement element = null;

	By AboutYourSelfPage = By.xpath("//span[ text() ='A few more questions about ']");
	

	By selectMale = By.xpath("//input[@type= 'radio' and @ value='M']//following::span[1]");
	

	By selectFemale = By.xpath("//*[@id=\"DriversAddPniDetails_embedded_questions_list_Gender_F\"]");

	By maritalStatus = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']");// (//select-input[@class='control'])[1]

	By SocialSecurityNumber = By.xpath("//input[@type='tel']");

	By primaryResidence = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']");

	By MovedInLast2Months = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']");

	By USLicenseStatus = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_LicenseStatus']");

	By yearsLicensed = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']");

	By chooseClaims = By.xpath("(//input[@value='N'])[2]");

	By chooseTickets = By.xpath("(//input[@value='N'])[3]");

	By continueBtm = By.xpath("//button[ text() ='Continue']");

	
	
	
	

	public ProgressiveAboutYourSelfClassObject(WebDriver driver) {

		this.driver = driver;

	}
	
	
	

	public void selectMale() throws InterruptedException {

		Thread.sleep(5000);

		driver.findElement(selectMale).click();
	

	}

	
	public void maritalStatus(String marital) throws InterruptedException

	{
		

		Thread.sleep(2000);

		WebElement maritalST = driver.findElement(maritalStatus);

		if (maritalST.getText().contains(marital)) {

			maritalST.sendKeys(marital);
			System.out.println(" MaritalStatus Option Selected ------- ");
			Thread.sleep(1100);

		}

	}

	public void SocialSecurityNumber(String SSNumber) throws InterruptedException {

		driver.findElement(SocialSecurityNumber).sendKeys(SSNumber);
		Thread.sleep(1100);

	}

	public void primaryResidence(String primResidence) throws InterruptedException

	{

		WebElement prmRes = driver.findElement(primaryResidence);

		if (prmRes.getText().contains(primResidence)) {

			prmRes.sendKeys(primResidence);
			System.out.println(" PrimaryResidence Option Selected ------- ");
			Thread.sleep(1100);

		}

	}

	public void MovedInLast2Months(String movedlstMths) throws InterruptedException

	{

		WebElement mvdlstMth = driver.findElement(MovedInLast2Months);

		if (mvdlstMth.getText().contains(movedlstMths)) {

			mvdlstMth.sendKeys(movedlstMths);
			System.out.println(" MovedInLast2Months Selected ------- ");
			Thread.sleep(1100);

		}

	}

	public void USLicenseStatus(String USLicnSts) throws InterruptedException

	{

		WebElement USLicenseStat = driver.findElement(USLicenseStatus);

		if (USLicenseStat.getText().contains(USLicnSts)) {

			USLicenseStat.sendKeys(USLicnSts);
			System.out.println(" USLicenseStatus Option Selected ------- ");
			Thread.sleep(1100);

		}

	}

	public void yearsLicensed(String YrsLicense) throws InterruptedException

	{

		WebElement LicenseYr = driver.findElement(yearsLicensed);

		if (LicenseYr.getText().contains(YrsLicense)) {

			LicenseYr.sendKeys(YrsLicense);
			System.out.println(" Driving Duration  Option Selected ------- ");
			Thread.sleep(1100);

		}

	}

	public void chooseClaims(String claim) throws InterruptedException {

		driver.findElement(chooseClaims).click();
		Thread.sleep(1100);

	}

	public void chooseTickets(String chTket) throws InterruptedException {

		driver.findElement(chooseTickets).click();
		Thread.sleep(1100);

	}

	public void continueBtm() throws InterruptedException {
		ScreenShotsClassObject.captureScreenShots(driver, "Driving History Page");
		driver.findElement(continueBtm).click();
		Thread.sleep(1100);

	}

}


