package Log4J;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Log4JDemo {

	

		
		static Logger logger = Logger.getLogger(Log4JDemo.class);
		
		
		

		public static void main(String[] args) 
		{

		// PropertiesConfigurator is used to configure logger from properties file
		PropertyConfigurator.configure("log4j.properties");
		
		

		// Log in console in and log file
		logger.debug("Log4J appender configuration is successful !!");
		
		

		// Log4J-Log levels
		logger.trace(" Trace: ---Most detailed information.");
		logger.debug("Debug:---- Deatiled inforamtion on the flow through the System. "
		+ "Expect these to be only written to Logs."
		+ "Generally speaking most lines logged by your application should be written as DEBUG");

		logger.info("INFO:--- for interesting runtime events(startup/shutdown)."
		+ "Expect these to be immediately visible on a console, so be conservative and keep to a minimum");

		logger.warn("WARN: ----Not necessarily wrong but sort of a warning printed in the console/File");
		logger.error(
		"Error:--other runtime errors or unexpected conditions.Expect these to be immediately seen on status console");
		logger.fatal("");
		// logger.off
		
		

		}

		
				
	}


